<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use OpenApi\Attributes as OA;

class UserController extends Controller
{
    #[OA\Get(
        tags: ['User'], path: '/user',
        description: 'get data user pagination with query',
        summary: 'get data user pagination with query',
        security: [['sanctum' => []]],
        parameters: [
            new OA\Parameter(description: 'per_page', in: 'query', name: 'per_page',
                example: '', schema: new OA\Schema(type: 'number', format: 'int64')),
            new OA\Parameter(description: 'pagination page', in: 'query', name: 'page',
                example: '', schema: new OA\Schema(type: 'number', format: 'int64')),
        ],
        responses: [
            new OA\Response(response: 200, description: 'OK',
                content: new OA\JsonContent(allOf: [
                    new OA\Schema(properties: [
                        new OA\Property(property: 'data', type: 'array',
                            items: new OA\Items(ref: '#/components/schemas/User')),
                    ]),
                    new OA\Schema(ref: '#/components/schemas/laraveResourcePagination'),
                ])
            ),
        ]
    )]
    public function index(Request $request)
    {
        $users = User::filterRequest($request);
        return UserResource::collection(
            $users->paginate(
                $request->per_page ?? 10)
        );
    }

    #[OA\Post(
        path: '/user', tags: ['User'], operationId: 'create user',
        summary: 'create User', description: 'create user',
        security: [['sanctum' => []]],
        responses: [
            new OA\Response(
                response: 200, description: 'OK',
                content: new OA\JsonContent(ref: '#/components/schemas/User')
            ),
        ],
        requestBody: new OA\RequestBody(
            content: new OA\JsonContent(ref: '#/components/schemas/User')
        )
    )]
    public function store(Request $request)
    {
        $this->validate($request, [
            'password' => ['required', 'min:6'],
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users']
        ]);
        $user = new User($request->toArray());
        $user->save();
        $user->assignRole('user');
        return new UserResource($user);
    }

    #[OA\Get(
        tags: ['User'],
        path: '/user/{id}',
        description: 'get single user',
        summary: 'get single user',
        security: [['sanctum' => []]],
        parameters: [
            new OA\Parameter(
                description: 'path id item',
                in: 'path',
                name: 'id',
                example: '',
                required: true,
                schema: new OA\Schema(type: 'string', format: 'text')
            )
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(properties: [
                    new OA\Property(property: 'data', allOf: [
                        new OA\Schema(ref: '#/components/schemas/User'),
                    ]),
                ])
            ),
        ]
    )]
    public function show(User $user)
    {
        return new UserResource($user);
    }

    #[OA\Put(
        tags: ['User'],
        path: '/user/{id}',
        description: 'update data user',
        security: [['sanctum' => []]],
        summary: 'update data user',
        parameters: [
            new OA\Parameter(
                description: 'path id item',
                in: 'path',
                name: 'id',
                example: '',
                required: true,
                schema: new OA\Schema(type: 'string', format: 'text')
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(ref: '#/components/schemas/User')
            ),
        ],
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(ref: '#/components/schemas/User')
        )
    )]
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'password' => ['sometimes', 'required', 'min:6'],
            'name' => ['required', 'sometimes'],
            'role' => ['required', 'sometimes'],
            'email' => ['sometimes', 'required', 'email', "unique:users,email,{$user->id}"]
        ]);
        $user = $user->fill($request->toArray());
        $user->save();
        $user = $user->refresh();
        if($request->has('role')){
            try {
                $user->syncRoles($request->role);
            } catch (\Throwable $th) {
                throw ValidationException::withMessages([
                    'message' => "Role {$request->role} does not exist",
                ]);
            }
        }
        return new UserResource($user);
        
    }

    #[OA\Delete(
        tags: ['User'],
        path: '/user/{id}',
        summary: 'soft delete user',
        description: 'soft delete user',
        security: [['sanctum' => []]],
        parameters: [
            new OA\Parameter(
                description: 'path id item',
                in: 'path',
                name: 'id',
                example: '',
                required: true,
                schema: new OA\Schema(type: 'string', format: 'text')
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(property: 'data', allOf: [
                            new OA\Schema(ref: '#/components/schemas/User'),
                        ]),
                    ]
                )
            ),
        ]
    )]
    public function destroy(User $user)
    {
        $user->delete();
        $user = $user->refresh();
        return new UserResource($user);
    }
}
