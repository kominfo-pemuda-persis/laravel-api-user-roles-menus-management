<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Attributes as OA;

#[OA\OpenApi(
    info: new OA\Info(title: 'Common API', version: '0.1-dev', description: 'Common API Documentation.'),
    servers: [
        new OA\Server(description: 'Development Server', url: '/api'),
    ]
)]
#[OA\Schema(
    xml: new OA\Xml(name: 'tokenResponse'),
    schema: 'tokenResponse',
    properties: [
        new OA\Property(property : 'token', type : 'email', format : 'text',
            example : '23|WsUUDfHz3G1fga9isKrf1ZIgh6oXYzJgCO4wtPOV'),
    ]
)]

#[OA\Schema(
    xml: new OA\Xml(name: 'loginRequest'),
    schema: 'loginRequest',
    required: ['email', 'password'],
    properties: [
        new OA\Property(property : 'email', type : 'email', format : 'text',
            example : 'test@example.com'),
        new OA\Property(property : 'password', type : 'string', format : 'text',
            example : 'password'),
    ]
)]

#[OA\Schema(
    xml: new OA\Xml(name: 'laravePagination'),
    schema: 'laravePagination',
    properties: [
        new OA\Property(property : 'current_page', type : 'number', format : 'int64',
            example : '1'),
        new OA\Property(property : 'first_page_url', type : 'string', format : 'text',
            example : 'http://localhost/api/user?page=1'),
        new OA\Property(property : 'from', type : 'number', format : 'int64',
            example : '1'),
        new OA\Property(property : 'per_page', type : 'number', format : 'int64',
            example : '1'),
        new OA\Property(property : 'to', type : 'number', format : 'int64',
            example : '1'),
        new OA\Property(property : 'total', type : 'number', format : 'int64',
            example : '10'),
        new OA\Property(property : 'last_page', type : 'number', format : 'int64',
            example : '1'),
        new OA\Property(property : 'next_page_url', type : 'string', format : 'text',
            example : 'http://localhost/api/user?page=1'),
        new OA\Property(property : 'last_page_url', type : 'string', format : 'text',
            example : 'http://localhost/api/user?page=1'),
        new OA\Property(property : 'path', type : 'string', format : 'text',
            example : 'http://localhost/api/user?page=1'),
    ]
)]

#[OA\Schema(
    xml: new OA\Xml(name: 'metaResource'),
    schema: 'metaResource',
    properties: [
        new OA\Property(property: 'current_page', type: 'number', example: '1'),
        new OA\Property(property: 'from', type: 'number', example: '1'),
        new OA\Property(property: 'last_page', type: 'number', example: '1'),
        new OA\Property(property: 'links', type: 'array', items: new OA\Items(properties: [
            new OA\Property(property: 'url', type: 'number', example: 'http://localhost/api/category?page=1'),
            new OA\Property(property: 'label', type: 'number', example: '1'),
            new OA\Property(property: 'active', type: 'number', example: 'true'),
        ]), example: [
            [
                'url' => null,
                'label' => '&laquo; Previous',
                'active' => false,
            ],
            [
                'url' => 'http://localhost/api/category?page=1',
                'label' => '1',
                'active' => true,
            ],
            [
                'url' => null,
                'label' => 'Next &raquo;',
                'active' => false,
            ],
        ]),
    ]
)]

#[OA\Schema(
    xml: new OA\Xml(name: 'linkResource'),
    schema: 'linkResource',
    properties: [
        new OA\Property(property: 'first', type: 'string', example: 'http://localhost/api/category?page=1'),
        new OA\Property(property: 'last', type: 'string', example: 'http://localhost/api/category?page=1'),
        new OA\Property(property: 'prev', type: 'string', example: 'http://localhost/api/category?page=1'),
        new OA\Property(property: 'next', type: 'string', example: 'http://localhost/api/category?page=1'),
    ]
)]

#[OA\Schema(
    xml: new OA\Xml(name: 'laraveResourcePagination'),
    schema: 'laraveResourcePagination',
    properties: [
        new OA\Property(property : 'meta', allOf: [
            new OA\Schema(ref: '#/components/schemas/metaResource'),
        ]),
        new OA\Property(property : 'links', allOf: [
            new OA\Schema(ref: '#/components/schemas/linkResource'),
        ]),
    ]
)]
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
