<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use OpenApi\Attributes as OA;

class AuthController extends Controller
{
    #[OA\Post(
        path: '/auth/login', tags: ['Auth'], operationId: 'login',
        summary: 'Authenticate User', description: 'Authenticate user',
        responses: [
            new OA\Response(response: 200, description: 'OK',
                content: new OA\JsonContent(ref: '#/components/schemas/tokenResponse')
            ),
        ],
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(ref: '#/components/schemas/loginRequest')
        )
    )]
    public function authenticateUser(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'message' => 'The provided credentials are incorrect.',
            ]);
        }

        return response()->json([
            'token' => $user->createToken(
                $request->email)->plainTextToken,
        ], Response::HTTP_OK);
    }

    #[OA\Get(
        tags: ['Auth'], path: '/auth/me', summary: 'Get logged in user profile',
        description: 'Get logged in user profile',
        security: [['sanctum' => []]],
        parameters: [
            new OA\Parameter(description: 'extend data to be queried', in: 'query', name: 'extend[]',
                schema: new OA\Schema(
                    type: 'array', items: new OA\Items(type: 'string'))),
        ],
        responses: [
            new OA\Response(response: 200, description: 'OK',
                content: new OA\JsonContent(properties: [
                    new OA\Property(property: 'data', allOf: [
                        new OA\Schema(ref: '#/components/schemas/User'),
                    ]),
                ])
            ),
        ]
    )]
    public function getAuthenticatedUser(Request $request): UserResource
    {
        $this->validate($request, [
            'extend' => ['sometimes', 'array'],
            'extend.*' => ['required'],
        ]);
        $user = $request->user();
        if ($request->has('extend')) {
            $user->load($request->extend);
        }

        return new UserResource($user);
    }
}
