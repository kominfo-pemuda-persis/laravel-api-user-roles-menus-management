<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use PragmaRX\Version\Package\Version;

class VersioningAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:version';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update app version';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->rewriteSwaggerVersion();
    }

    public function rewriteSwaggerVersion(): string
    {
        Artisan::call('l5-swagger:generate');
        Artisan::call('version:absorb');
        $version = new Version();
        $format = $version->format('timestamp-full');
        $jsonString = Storage::disk('api-docs')->get('api-docs.json');
        $data = json_decode($jsonString, true);
        $time = Carbon::parse($format);
        $time = $time->setTimezone('+7')
            ->format('d M Y H:i:s');
        $data['info']['version'] = config('app.env') === 'production' ?
            $version->version() : $version->format('dev');
        $data['info']['description'] = "Common API Documentation. Last update: {$time} (UTC+7)";
        $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
        $this->alert("New version {$time} -> {$data['info']['version']}");
        Storage::disk('api-docs')->put(
            'api-docs.json', stripslashes($newJsonString));

        return $jsonString;
    }
}
