<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use OpenApi\Attributes as OA;
use Spatie\Permission\Traits\HasRoles;

#[OA\Schema(
    required: ['name', 'email', 'password'],
    xml: new OA\Xml(name: 'User'),
    properties: [
        new OA\Property(property : 'name', type : 'string', format : 'text', example : 'Jhon Doe'),
        new OA\Property(property : 'email', type : 'email', format : 'text', example : 'test@example.com'),
        new OA\Property(property : 'password', type : 'string', format : 'text', example : 'password'),
    ]
)]
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, HasUuids, SoftDeletes, Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function scopeFilterRequest(Builder $queryBuilder, Request $request): void
    {
        $filters = [
            'id' => fn ($query) => $query->where('id', $request->id),
            'email' => fn ($query) => $query->where('email', $request->email),
            'username' => fn ($query) => $query->where('username', $request->username),
            'name' => fn ($query) => $query->where('name', 'like', "%{$request->name}%"),
            'phone' => fn ($query) => $query->whereIn('phone', $request->phone),
            'extend' => fn ($query) => $query->with($request->extend),
        ];

        foreach ($filters as $param => $filter) {
            if ($request->has($param)) {
                $filter($queryBuilder);
            }
        }

    }
}
