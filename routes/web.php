<?php

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use PragmaRX\Version\Package\Version;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $versionState = new Version();
    $version = config('app.env') === 'production' ?
        $versionState->version() : $versionState->format('dev');

    $time = Carbon::parse(
        $versionState->format('timestamp-full'));
    $swagger = config('l5-swagger.default');
    $swagger = config("l5-swagger.documentations.$swagger");
    $swagger_url = url("{$swagger['routes']['api']}");
    $swagger_title = "{$swagger['api']['title']}";

    return response()->json([
        'data' => "$swagger_title $version",
        'swagger_url' => $swagger_url,
        'release_date_indonesia' => $time->setTimezone('+7')
            ->format('d M Y H:i:s'),
        'release_date_utc' => $time->setTimezone('UTC')
            ->format('d M Y H:i:s'),
        'version' => $version,
    ], Response::HTTP_OK);
});
