<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Seeders\DefaultRoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserAPITest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    /**
     * A basic feature test example.
     */

    protected function setUp(): void {
        parent::setUp();
        $this->seed(DefaultRoleSeeder::class);
    }

    public function test_create_user_then_login(): void
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $user->assignRole('super-admin');
        $updatedName = $this->faker()->name;
        $updatedEmail = $this->faker()->email;
        $updatedPassword = $this->faker()->password();
        $payload = [
            'password' => $updatedPassword,
            'name' => $updatedName,
            'email' => $updatedEmail
        ];
        $response = $this->postJson('/api/user', $payload);
        $response->assertCreated();
        $this->assertDatabaseCount(User::class, 2);
        $response = $this->postJson("/api/auth/login", $payload);
        $response->assertSuccessful();
    }

    public function test_update_user_then_login(): void
    {
        $user = User::factory()->create();
        $updatedName = $this->faker()->name;
        $updatedEmail = $this->faker()->email;
        $updatedPassword = $this->faker()->password();
        $updatedPayload = [
            'password' => $updatedPassword,
            'name' => $updatedName,
            'role' => 'super-admin',
            'email' => $updatedEmail
        ];
        $this->actingAs($user);
        $user->assignRole('super-admin');
        $response = $this->putJson("/api/user/{$user->id}", $updatedPayload);
        $response->assertSuccessful();
        $this->assertDatabaseHas(User::class, [
            'name' => $updatedName,
            'email' => $updatedEmail
        ]);
        $response = $this->postJson("/api/auth/login", $updatedPayload);
        $response->assertSuccessful();
    }

    public function test_soft_delete_user(): void
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $user->assignRole('super-admin');
        $user2 = User::factory()->create();
        $response = $this->deleteJson("/api/user/{$user2->id}");
        $response->assertSuccessful();
        $user2 = $user2->refresh();
        $deleted_at = $user2->deleted_at;
        $this->assertDatabaseCount(User::class, 2);
        $this->assertDatabaseHas(User::class, [
            'deleted_at' => $deleted_at
        ]);
    }

    public function test_get_single_user(): void
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $user->assignRole('super-admin');
        $response = $this->getJson("/api/user/{$user->id}");
        $response->assertJsonFragment([
            'name' => $user->name,
            'email' => $user->email
        ]);
    }
}
