<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create([
            "name" => "super-admin"
        ]);
        Role::create([
            "name" => "user"
        ]);
        Role::create([
            "name" => "guest"
        ]);
    }
}
